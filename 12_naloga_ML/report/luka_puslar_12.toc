\babel@toc {slovene}{}
\contentsline {section}{\numberline {1}Uvod}{2}{section.1}%
\contentsline {section}{\numberline {2}Strojno učenje}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Analiza podatkov}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}BDT}{5}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Primerjava glede na vhodne podatke}{6}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Primerjava glede na parametre modela}{8}{subsubsection.2.2.2}%
\contentsline {subsection}{\numberline {2.3}DNN}{11}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Primerjava glede na vhodne podatke}{12}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}Primerjava glede na parametre modela}{12}{subsubsection.2.3.2}%
\contentsline {section}{\numberline {3}Ugotovitve}{18}{section.3}%
\contentsline {section}{\numberline {4}Zaključek}{18}{section.4}%
