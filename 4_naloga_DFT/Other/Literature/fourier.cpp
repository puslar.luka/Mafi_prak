// =============================================================================
//   Implementacija pocasne splosne DFT
//   H_m = \sum_n=0^{N-1} h_n exp(I 2pi n m/N) , m = 0,.., N-1
//
//   prevanjanje:
//    g++ fourier.cpp -o fourier -lfftw3
//
// Avtor: Martin Horvat, 2004
// =============================================================================

#include <iostream>
#include <cmath>
#include <complex>
#include <iomanip>

#include <ctime>
#include <cstdlib>
#include <cstdio>

// version 3.x fftw
#include <fftw3.h>   

using namespace std;

#define dcomplex complex <double>

int main(int argc, char *argv[]){

 if (argc != 5){
    cerr << "Usage: fourier <+,-> <r,c> <n,f> <Npoints> < <in_r,in_c> > <out_c> \n\n"
         << "Vedi:  +,- - Fourier-va ali Inverzna Fourierova transformacija\n"
	 << "       r,c - vhodni podatki so realni ali compleksni\n"
	 << "       n,f - not-fast FT, fast FT\n"; 
    exit(1);
  }
  
  
  long N = atoi(argv[4]),
       i, j;
  
  double re, im;
  
  dcomplex *d = new dcomplex [N],
          w, wn, wnm, tmp; 
  
  cout << setprecision(16);
   
  if (argv[2][0]=='r')
    for (i = 0; i < N; i++) cin >> d[i];
  else 
    for (i = 0; i < N; i++) {
      cin >> re >> im;
      d[i] = dcomplex (re,im);
    }  

  if (argv[3][0]=='n'){  // NOT-FAST FT
  
    if (argv[1][0]=='+')
       w = dcomplex(cos(2*M_PI/N), sin(2*M_PI/N));
    else 
       w = dcomplex(cos(2*M_PI/N), -sin(2*M_PI/N)); 
     
    wn = 1;  
    
    for (i = 0; i < N; i++){
    
      tmp = 0; 
      wnm = 1;
    
      for (j = 0; j < N; j++) {
        tmp += d[j]*wnm;
        wnm *= wn;
      }  
    
      cout << real(tmp) << '\t' << imag(tmp) << '\n';  
    
      wn *= w;
    }
  
  } else {		// FAST FT
    
    fftw_plan plan;
    
    if (argv[1][0]=='+')
      plan = fftw_plan_dft_1d(N, 
            (fftw_complex*)d, (fftw_complex*)d, 
             FFTW_FORWARD, FFTW_ESTIMATE);       
    else 
      plan = fftw_plan_dft_1d(N, 
             (fftw_complex*)d, (fftw_complex*)d,
             FFTW_BACKWARD, FFTW_ESTIMATE);
	     
    fftw_execute(plan);	     
    
    for (i = 0; i < N; i++)
      cout << real(d[i]) << '\t' << imag(d[i]) << '\n';  
    
    fftw_destroy_plan(plan);  
    
  }
  
  return 0;
}
