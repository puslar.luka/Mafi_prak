// ============================================================================
//  Uvod v C++ complex class
//
// Avtor: Martin Horvat, 2004
// ============================================================================

#include <iostream>
#include <cmath>
#include <complex>


using namespace std;

typedef complex<double>  dcomplex; 	// ali 
// #define dcomplex complex<double>  

int main(){

  // imamo dve moznosti polnjenja variabel kompleksnega tipa 
  dcomplex c(0,1) , d;
  
  d = dcomplex(1,1);
  
  
  // na kompleksnim tipom lahko izvajamo vse algebricne operacije
  // kot na realnem tipu
  
  dcomplex e, f;
  
  e = c*d,  
  f = c/d;
  
  // posebaj za ta tip pa definirane se funkcije 
  // real(), imag() in norm() == abs()^2
  
  cout << "Prvo stevilo:"  << c << '\n'
       << "Drugo stevilo:" << d << '\n' 
       << "Tretje stevilo:" << f << ' '
       << "sestavljeno iz:" << real(f)  << ' ' << imag(f) << '\n';
  
  
  // seveda je operator << tukaj tudi preoklikovan posebaj za compleksni tip, 
  // kot veliko drugih funckij npr. abs() itd. vendar se ni dobro zanasati
  // na to 
  return 0;
}

