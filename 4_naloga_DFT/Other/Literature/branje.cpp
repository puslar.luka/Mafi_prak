// ============================================================================
//  Branje enostavnih WAV (PCM WAVE) datotek z 16 bitnimi podatki
//  Uporaba: branje <file.wav> > <file.txt>
//
//  Avtor: Martin Horvat, 2004
// =============================================================================

#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]){
  
  if (argc != 2) {
    cerr << "Uporaba: branje <file.wav> > <file.txt>\n";
    exit(1);
  }
    
  ifstream ifile(argv[1]);

  char b, c[5];
  
  int i = 0; 
  
  short w;
  
  while (1)  {
    
    ifile.read(&b, 1);
    
    if (i < 4) 
      c[i] = b; 
    else {
      c[0] = c[1]; 
      c[1] = c[2];
      c[2] = c[3]; 
      c[3] = b;
            
      if (c[0]=='d' && c[1]=='a' && c[2] == 't' && c[3] == 'a') break;
    }
    i++; 
  }
  
  ifile.read(c, 4);	// skipping chunksize
	  
  while (ifile){
    ifile.read((char *)&w, 2);
    if (ifile) cout << w << '\n';      
  } 
    
  return 0;
}
