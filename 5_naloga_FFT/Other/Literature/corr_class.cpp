//=============================================================================
// Program for calculating auto-correlation of real data
// No normalization to reduce computational cost and data aliasing!
//
// Compile:
// g++ corr.cpp -o corr -O3 -Wall -lm -lfftw3
// Author: Martin Horvat, 2007
// ============================================================================

#include <iostream>
#include <cmath>
#include <complex>
#include <queue>

using namespace std;

// my corr library
#include "corr_class.h"
			
int main(int argc, char *argv[]){
  
  if (argc != 3){
    cerr << "Usage: ./corr <method> <M> < input > output 2> <mean>\n\n"
         << "Note:\n"
	 << "  method:\n"
	 << "    s - simple with FFT\n"
	 << "    c - correct with FFT\n"
	 << "    d - direct by def without FFT\n"
         << "  M - size of data used for a single calculation\n"
         << "      if M = 0 -- whole set is taken\n"
         << "      if M > 1 -- result final result is an average\n"
         << "                  over all possible sets\n" 
         << "  mean -- average calculated\n"
         << "  std -- standard deviation\n"; 
 
    exit(1);
  }
  
  char model = argv[1][0];
   
  int i, M = atoi(argv[2]);

  double f, *d = NULL;

  Tcorr *corr = NULL;

  cout << "#model=" << model << '\n' << "#M="  << M << '\n';
      
  cerr.precision(12); 
  cout.precision(12);   
   
  if (M == 0){
    queue<double> data;
       
    while (cin >> f) data.push(f);
    
    M = data.size();
    
    switch (model){
      case 's': corr = new Tcorr_simple(d, M); break;
      case 'c': corr = new Tcorr_correct(d, M); break;
      case 'd': corr = new Tcorr_def(d, M); break;
      default: cerr << "This correlation model is not supported\n";
    }
        
    for (i = 0; !data.empty(); ++i) {
      d[i] = data.front();
      data.pop();
    }
    
    // mean 
    cerr << corr -> calc() << '\n';
    
    // correlation function
    for (i = 0; i < M; i++) cout << d[i] << '\n';
        
  } else {
    
    double *e = NULL;
    
    switch (model){
      case 's': corr = new Tcorr_simple(d, e, M); break;
      case 'c': corr = new Tcorr_correct(d, e, M); break;
      case 'd': corr = new Tcorr_def(d, e, M); break;
      default: cerr << "This correlation model is not supported\n";
    }
    
    int K = 1;
    
    double *g = new double [M],
           *g_dev = new double [M],
           *p, *d_end = d + M - 1;
    
    memset (d, 0, sizeof(double)*M);
     
    for (i = 0; i < M && cin >> f; ++i) d[i] = f;
    
    Tstat mean;
    
    mean.add(corr->calc());
       
    memcpy(g, e, sizeof(double)*M);
      
    for (i = 0; i < M; ++i) g_dev[i] = e[i]*e[i];
    
    for (;cin >> f; ++K) {
      
      //moving memory
      for (p = d; p != d_end; ++p) *p = *(p+1);
      *d_end = f;
      
      // calculating correlation
      mean.add(corr->calc());  
      
      for (i = 0; i < M; ++i) { 
        g[i] += e[i]; 
        g_dev[i] += e[i]*e[i];
      }
      cerr << "#K=" << K << endl;    
    }
       
    for (i = 0; i < M; ++i) {g[i] /= K; g_dev[i] = g_dev[i]/K - g[i]*g[i];}

    mean.calc();
    
    // mean +- delta_mean, standard devitation of the mean
    cerr << mean.mean << '\t' << mean.dev << '\t' << K << '\n';
    
    // average correlation and it standard deviation
    for (i = 0; i < M; i++) cout << g[i] << '\t' << g_dev[i] << '\n';

    delete [] g;
    delete [] g_dev;
  }
  
   
  delete corr;
  
  return 0;
}
