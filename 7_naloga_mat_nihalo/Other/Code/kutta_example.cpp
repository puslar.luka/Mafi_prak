/* =============================================================================

  Primer procedure uporabe Runge-Kutta cetrtega reda integratorja
  
    x' = Fun[t,x], x = vektor dimenzije n

  Prevajanje:
    g++ kutta_example.cpp -o kutta_example -Wall -O3 
  ali
    g++ kutta_example.cpp -o kutta_example -Wall -O3 -pipe -fomit-frame-pointer\
        -ffast-math -malign-double -funroll-loops -Wno-deprecated
	
 Avtor: Martin Horvat, 2004
 ============================================================================= */

#include <iostream>
#include <cmath>

using namespace std;

// ===========================================================================
//  Funkcija odvoda za primer matematicnega nihala
//
// Input: t - cas
//        x - tocka v faznem prostoru dimenzije "n"
// Output: F - odvod 

void Fun(int n, double t, double *x, double *F){
  F[0] = x[1];
  F[1] = -sin(x[0]); 
} 

// ===========================================================================
// Neoptimizirana realizacija RK4
//
// Input: h - korak
//        t - cas
// Input/Output: x_ - tocka v faznem prostoru dimenzije "n"	

void RK4(int n, double h, double t, double *x_){

  long i;
  
  double *F = new double [n],
         *x = new double [n],
         *k1 = new double [n],
	 *k2 = new double [n],
	 *k3 = new double [n];
	 
  Fun(n, t, x_, F);
  
  for (i = 0; i < n; i++) {
    k1[i]= h*F[i];
    x [i] = x_[i] + 0.5*k1[i]; 
  }  
  
  Fun(n, t + 0.5*h, x, F);
  
  for (i = 0; i < n; i++) {
    k2[i]= h*F[i];
    x [i] = x_[i] + 0.5*k2[i]; 
  }  

  Fun(n, t + 0.5*h, x, F);
 
  for (i = 0; i < n; i++) {
    k3[i]= h*F[i];
    x [i] = x_[i] + k3[i]; 
  }  

  Fun(n, t + h, x, F);
 
  for (i = 0; i < n; i++)
    x_[i] += (k1[i] + 2*k2[i] + 2*k3[i] + h*F[i])/6; 
 
  
  delete [] F;
  delete [] k1;
  delete [] k2;
  delete [] k3;

}

// =============================================================================
//				Glavni program 
// =============================================================================

int main(int argc, char * argv[]){
  
  if (argc != 5){
    cerr << "Uporaba: kutta_example <h> <T> <x0> <p0> > <trajektorija> \n"
         << "         h - korak\n"
	 << "         T - casovni interval\n"
	 << "         x0,p0 - zacetna pozicija in hitrost\n";
	             
    exit(1);
  }
  
  int n = 2, N; 	// 2D sistem
  
  double h = atof(argv[1]),
         T = atof(argv[2]),
         *xin = new double [2]; 
  
  xin[0] = atof(argv[3]);
  xin[1] = atof(argv[4]); 
  
  N = int(T/h)+1; 	// stevilo iteracijskih korakov
  
  for (int i = 0; i < N; i++){
    RK4(n, h, 0, xin);
    cout << h*i << '\t' <<  xin[0] << '\t' << xin[1] << '\n';    
  }

  delete [] xin;
  return 0;
}
