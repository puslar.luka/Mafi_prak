/* 

 Demonstracija rutin za diagonalizacijo simetricnih matrix iz
 zbirke Numerical recipes.
 C++ tipa -- null determined array
 
 Prevajanje: g++ diag.cpp -o diag -O2
 
 Avtor: Martin Horvat, december 2007
*/

#include <iostream>
#include <cmath>

// ***************************** Deklaracije **********************************

/* Redukcija realne simetricne matrike na tridiagonalno in prehodno matriko

 input: a - realna simetricna matrika 
 output: a -- prehodna matrika
         d- diagonala
         e - zgornja diagonala, kjer je e[0] poljuben        
*/

void  tred2(double **a, int n, double *d, double *e); 

/* Resi lastnem system simetricne tridiagonane matrike, ki je 
  podana z diagonalo d[] in zgornjo diagonalo e[]
  
  input: d - diagonala
         e - zgornja diagonala, kjer je e[0] poljuben
         z - prehodna matrika oz. identiteta z[i][j]=delta_{i,j}
  output: d-- lastne vrednosti,
          z - lastni vektorji v kolonah
*/
void  tqli(double *d, double *e, int n, double **z);

/* Resi lastnem system simetricne tridiagonane matrike, ki je 
  podana z diagonalo d[] in zgornjo diagonalo e[]
  
  input: d - diagonala
         e - zgornja diagonala, kjer je e[0] poljuben
  output: d-- lastne vrednosti,
*/

void  tqli(double *d, double *e, int n);

// defintions of matrices (C/C++ style -- null determined)
template <class T> T** matrix(long nrow, long ncol);
template <class T> void free_matrix(T **m);

// **************************** Glavni program ********************************

int main(){
  
  long n = 3, i, j;
  
  double **A = matrix <double> (n,n), // 3x3
         *d = new double [n],
	      *e = new double [n]; 
  
  A[0][0]=1; A[0][1]=0; A[0][2]=2; 
  A[1][0]=0; A[1][1]=1; A[1][2]=4;  
  A[2][0]=2; A[2][1]=4; A[2][2]=1;
  
  tred2(A, n, d, e);
  tqli (d, e, n, A);
  
  std::cout << "Lastne vrednosti:\n";
  for (i = 0; i < n; i++) std::cout << i << '\t' << d[i] << '\n';

  std::cout << "Lastni vektorji:\n";
  for (i = 0; i < n; i++) {
    std::cout << i << " )";
    for (j = 0; j < n; j++) std::cout <<'\t' << A[j][i];
    std::cout << '\n';
  }  
  
  delete [] d;
  delete [] e;
  
  free_matrix <double> (A);

  return EXIT_SUCCESS;
}

// ******************************* Rutine **********************************

// rezervacija matrike
template <class T> T** matrix (long nrow, long ncol) {

  T **m = new T* [nrow];
  m[0] = (T *) new char [nrow*ncol*sizeof(T)];
  for( long i = 1; i < nrow; i++) m[i] = m[i-1] + ncol;

  return m;	
}

// sprostitev matrike
template <class T> void free_matrix(T **m) {
  delete [] (char*) m[0];
  delete [] m;
}

inline double sqr(double f){ return f*f; }


inline double pythag(double a, double b) {

  double absa = std::abs(a), absb = std::abs(b);
  
  if (absa > absb) return absa*sqrt(1.0 + sqr(absb/absa));
  
  return (absb == 0.0 ? 0.0 : absb*sqrt(1.0 + sqr(absa/absb)));
}

void tred2(double **a, int n, double *d, double *e) {

  int l, k, j, i;
  
  double scale, hh, h, g, f;

  for (i = n-1; i > 0; i--) {
    l = i-1;
    h = scale = 0.0;
    if (l > 0) {
      for (k = 0; k <= l; k++)
        scale += std::abs(a[i][k]);
	if (scale == 0.0)
          e[i] = a[i][l];
	else {
	  for (k = 0; k <= l;k++) {
	    a[i][k] /= scale;
	    h += a[i][k]*a[i][k];
	}
	f = a[i][l];
	g = (f >= 0.0 ? -sqrt(h) : sqrt(h));
	e[i] = scale*g;
	h -= f*g;
	a[i][l] = f-g;
	
	f = 0.0;
	for (j = 0; j <= l; j++) {
  	  a[j][i] = a[i][j]/h;
	  g = 0.0;
	
	  for (k = 0; k <= j; k++) g += a[j][k]*a[i][k];
	  
	  for (k=j+1;k<=l;k++) g += a[k][j]*a[i][k];
	  
	  e[j] = g/h;
	  f += e[j]*a[i][j];
	}
	
	hh = f/(h+h);
	for (j = 0; j <= l; j++) {
	  f = a[i][j];
	  e[j] = g = e[j] - hh*f;
	  
	  for (k = 0; k <= j; k++) a[j][k] -= (f*e[k]+g*a[i][k]);
	}
      }
    } else e[i]=a[i][l];
    
    d[i]=h;
  }
  
  d[0]=0.0;
  e[0]=0.0;
  
  for (i = 0; i < n; i++) {
    l = i-1;
    if (d[i]) {
      for (j = 0; j <= l; j++) {
        g = 0.0;
        for (k = 0; k <= l; k++) g += a[i][k]*a[k][j];
        for (k = 0; k <= l; k++) a[k][j] -= g*a[k][i];
      }
    }
    d[i] = a[i][i];
    a[i][i] = 1.0;
    for (j = 0; j <= l; j++) a[j][i] = a[i][j] = 0.0;
  }
}

#define sign_(a,b) ((b) >= 0.0 ? std::abs(a) : -std::abs(a))

void tqli(double *d, double *e, int n, double **z) {
  
  int m, l, iter, i, k;
  
  double s, r, p, g, f, dd, c, b;

  for (i =1; i < n; i++) e[i-1] = e[i];
  
  e[n-1] = 0.0;
    
  for (l = 0; l < n; l++) {
    
    iter = 0;
  
    do {
      for (m = l; m < n - 1; m++) {
        dd = std::abs(d[m]) + std::abs(d[m+1]);
        if ((double)(std::abs(e[m]) + dd) == dd) break;
      }
      
      if (m != l) {
        
        if (iter++ == 30) {
          std::cerr << "Too many iterations in tqli";
	        exit(EXIT_FAILURE);
        }  
	
        g = (d[l+1] - d[l])/(2.0*e[l]);
        r = pythag(g,1.0);
        g = d[m] - d[l] + e[l]/(g + sign_(r,g));
        s = c = 1.0;
        p = 0.0;
      
        for (i = m-1; i >= l; i--) {
          f = s*e[i];
          b = c*e[i];
      	  e[i+1] = (r = pythag(f,g));
      	
      	  if (r == 0.0) {
      	    d[i+1] -= p;
      	    e[m] = 0.0;
      	    break;
      	  }
      	  s = f/r;
      	  c = g/r;
      	  g = d[i+1] - p;
      	  r = (d[i] - g)*s + 2.0*c*b;
      	  d[i+1] = g + (p = s*r);
      	  g = c*r - b;
	  
          for (k = 0; k < n; k++) {
      	    f = z[k][i+1];
      	    z[k][i+1] = s*z[k][i] + c*f;
      	    z[k][i] = c*z[k][i] - s*f;
      	  }
        }
        if (r == 0.0 && i >= l) continue;
	
        d[l] -= p;
        e[l] = g;
        e[m] = 0.0;
      }
    } while (m != l);
  }
}

void tqli(double *d, double *e, int n) {
  
  int m, l, iter, i, k;
  
  double s, r, p, g, f, dd, c, b;

  for (i =1; i < n; i++) e[i-1] = e[i];
  
  e[n-1] = 0.0;
    
  for (l = 0; l < n; l++) {
    
    iter = 0;
  
    do {
      for (m = l; m < n - 1; m++) {
        dd = std::abs(d[m]) + std::abs(d[m+1]);
        if ((double)(std::abs(e[m]) + dd) == dd) break;
      }
      
      if (m != l) {
        
        if (iter++ == 30) {
          std::cerr << "Too many iterations in tqli";
	        exit(EXIT_FAILURE);
        }  
	
        g = (d[l+1] - d[l])/(2.0*e[l]);
        r = pythag(g,1.0);
        g = d[m] - d[l] + e[l]/(g + sign_(r,g));
        s = c = 1.0;
        p = 0.0;
      
        for (i = m-1; i >= l; i--) {
          f = s*e[i];
          b = c*e[i];
	        e[i+1] = (r = pythag(f,g));
	
      	  if (r == 0.0) {
      	    d[i+1] -= p;
      	    e[m] = 0.0;
      	    break;
      	  }
      	  s = f/r;
      	  c = g/r;
      	  g = d[i+1] - p;
      	  r = (d[i] - g)*s + 2.0*c*b;
      	  d[i+1] = g + (p = s*r);
      	  g = c*r - b;	  
        }
        
        if (r == 0.0 && i >= l) continue;
	
        d[l] -= p;
        e[l] = g;
        e[m] = 0.0;
      }
    } while (m != l);
  }
}

#undef sign_
